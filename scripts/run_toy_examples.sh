#!/bin/bash

# Output folder for individual results
output_folder="../result_toy_examples"

# Check if the output folder exists, if not create it
if [ ! -d "$output_folder" ]; then
  mkdir -p "$output_folder"
fi

# Loop through graphs 1 to 12
for i in {1..12}; do
  # Name of the .dot file based on the index
  input_file="../toys_examples/graph$i.dot"

  # Define an output file for each graph result
  result_file="$output_folder/result_graph$i.txt"

  # Write a separator line in the result file for each graph
  echo "-------------Graph$i-------------" > "$result_file"

  # Execute the ./optlp command and directly store the result in the respective file
  ./optlp --input-file "$input_file" --output-folder "$output_folder" >> "$result_file" 2>&1

  # Add an empty line to separate results for each graph
  echo "" >> "$result_file"
done

echo "All graphs have been processed, and individual result files have been created in $output_folder"
