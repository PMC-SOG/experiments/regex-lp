#!/bin/bash

# Output directory where all the individual result files will be stored
output_dir="../result_random_graphs"

# Check if the output directory exists, if not create it
if [ ! -d "$output_dir" ]; then
  mkdir -p "$output_dir"
fi

# First set of commands for 1000 nodes
for edges in 1000 1200 1250; do
  output_file="$output_dir/graph_1000_nodes_${edges}_edges.txt"
  echo "Running ./optlp for 1000 nodes and $edges edges"
  ./optlp --nb-nodes 1000 --nb-edges $edges --nb-labels 500 > "$output_file"
  echo "Results saved to $output_file"
done

# Second set of commands for 2000 nodes
for edges in 2000 2400 2600; do
  output_file="$output_dir/graph_2000_nodes_${edges}_edges.txt"
  echo "Running ./optlp for 2000 nodes and $edges edges"
  ./optlp --nb-nodes 2000 --nb-edges $edges --nb-labels 500 > "$output_file"
  echo "Results saved to $output_file"
done

# Third set of commands for 3000 nodes
for edges in 3000 3400 3700; do
  output_file="$output_dir/graph_3000_nodes_${edges}_edges.txt"
  echo "Running ./optlp for 3000 nodes and $edges edges"
  ./optlp --nb-nodes 3000 --nb-edges $edges --nb-labels 1000 > "$output_file"
  echo "Results saved to $output_file"
done

# Fourth set of commands for 4000 nodes
for labels in 4000 4400 4600; do
  output_file="$output_dir/graph_4000_nodes_3000_edges_${labels}_labels.txt"
  echo "Running ./optlp for 4000 nodes, 3000 edges, and $labels labels"
  ./optlp --nb-nodes 4000 --nb-edges 3000 --nb-labels $labels > "$output_file"
  echo "Results saved to $output_file"
done

echo "All graphs have been processed and individual result files created in $output_dir"
